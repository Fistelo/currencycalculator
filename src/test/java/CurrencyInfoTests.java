import org.junit.Before;
import org.junit.Test;
import sample.rmul.data.CurrencyInfo;
import sample.rmul.data.CurrencyTable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CurrencyInfoTests {

    List<CurrencyTable> acceptableCurrencies;
    LocalDate dateFrom;
    LocalDate dateTo;

    @Before
    public void initialize(){
        dateTo = LocalDate.of(2005, 4, 10);
        dateFrom = LocalDate.of(2005, 4,9);
        acceptableCurrencies = new ArrayList<>();
        CurrencyTable currencyTable = new CurrencyTable('A');
        currencyTable.getTable().add("PLN");
        currencyTable.getTable().add("USD");
        CurrencyTable currencyTable2 = new CurrencyTable('C');
        currencyTable.getTable().add("EUR");
        acceptableCurrencies.add(currencyTable);
        acceptableCurrencies.add(currencyTable2);
    }

    @Test
    public void validatedDataTest(){
        CurrencyInfo currencyInfo = new CurrencyInfo(dateFrom, dateTo,"PLN", acceptableCurrencies);
        assertEquals(currencyInfo.isDataValidated(), true);
    }
    @Test
    public void dateOrderValidationTest(){
        CurrencyInfo currencyInfo = new CurrencyInfo(dateTo, dateFrom, "PLN", acceptableCurrencies);
        assertEquals(currencyInfo.isDataValidated(), false);
    }
    @Test
    public void currencyCodeExistenceTest(){
        CurrencyInfo currencyInfo = new CurrencyInfo(dateFrom, dateTo,"INNY", acceptableCurrencies);
        assertEquals(currencyInfo.isDataValidated(), false);
    }
    @Test
    public void nullValueValidationTest(){
        CurrencyInfo currencyInfo = new CurrencyInfo(dateFrom, null,"PLN", acceptableCurrencies);
        assertEquals(currencyInfo.isDataValidated(), false);
    }
    @Test
    public void whenNoRecordInCurrencyTableThenReturnEmptyCollectionTest(){
        CurrencyInfo currencyInfo = new CurrencyInfo(dateFrom, dateTo, "INNY", acceptableCurrencies);
        assertEquals(currencyInfo.getTableNames(), Collections.emptyList());
    }
    @Test
    public void whenMatchInCurrencyTableThenReturn(){
        CurrencyInfo currencyInfo = new CurrencyInfo(dateFrom, dateTo, "EUR", acceptableCurrencies);
        assertNotEquals(currencyInfo.getTableNames(), Collections.emptyList());
    }
}

