package sample.rmul.calculator;

public interface CurrencyOperations {
    double sellStandardDeviation();
    double buyAverageRate();
}
