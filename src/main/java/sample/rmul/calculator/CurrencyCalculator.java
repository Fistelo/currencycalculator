package sample.rmul.calculator;

import jdk.incubator.http.HttpResponse;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.json.JSONArray;
import org.json.JSONObject;
import sample.rmul.Consts;
import sample.rmul.data.CurrencyInfo;
import sample.rmul.utilities.HttpRequestUtils;


public class CurrencyCalculator implements CurrencyOperations {

    private CurrencyInfo currencyInfo;

    private double [] sellRates;
    private double [] buyRates;
    private boolean dataDownloaded;

    public CurrencyCalculator(CurrencyInfo currencyInfo) {
        this.currencyInfo = currencyInfo;
        dataDownloaded = false;
    }

    private void getCTableData(){
        String request = new StringBuilder(Consts.API_LOCATION).append("exchangerates/rates/C/")
                .append(currencyInfo.getCurrencyCode()).append("/").append(currencyInfo.getDateFrom())
                .append("/").append(currencyInfo.getDateTo()).toString();
        HttpResponse<String> responseData = HttpRequestUtils.getRequest(request);
        if(responseData.statusCode() == Consts.HTTP_RESPONSE_OK) {
            parseCTableData(responseData.body());
        }
    }

    private void parseCTableData(String data){
        JSONArray rates = new JSONObject(data).getJSONArray("rates");
        sellRates = new double[rates.length()];
        buyRates = new double[rates.length()];

        for(int i =0; i<rates.length() ; i++){
            sellRates[i] = rates.getJSONObject(i).getDouble("ask");
            buyRates[i] = rates.getJSONObject(i).getDouble("bid");
        }
        dataDownloaded = true;
    }

    @Override
    public double sellStandardDeviation() {
        if(isTableReady()){
            StandardDeviation standardDeviation = new StandardDeviation(false);
            return standardDeviation.evaluate(sellRates);
        }
        return -1;
    }

    @Override
    public double buyAverageRate() {
        if(isTableReady()){
            Mean mean = new Mean();
            return mean.evaluate(buyRates);
        }
        return -1;
    }

    private boolean isTableReady(){
        if(!dataDownloaded) getCTableData();
        if(currencyInfo.getTableNames().contains('C')
                && dataDownloaded){
            return true;
        }
        return false;
    }
}
