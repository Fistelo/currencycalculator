package sample.rmul;

import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import sample.rmul.calculator.CurrencyCalculator;
import sample.rmul.data.CurrencyInfo;
import sample.rmul.data.CurrencyTable;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Controller {

    private static final String VALIDATION_ERROR = "Niepoprawne dane wejściowe!";
    private static final String RESULT_ERROR = "W API nie ma informacji dla tych danych.";

    private static final NumberFormat doubleFormatter = new DecimalFormat("#0.0000");
    private List<CurrencyTable> currencyTables;

    @FXML private TextField currencyCode;
    @FXML private DatePicker dateTo;
    @FXML private DatePicker dateFrom;
    @FXML private TextArea resultArea;

    public Controller() {
        prepareCurrencyTables();
    }

    @FXML
    private void calculateResult() {
        final CurrencyInfo data = new CurrencyInfo(dateFrom.getValue(),  dateTo.getValue(),
                currencyCode.getText().toString(), currencyTables);

        if(data.isDataValidated()){
            CurrencyCalculator calc = new CurrencyCalculator(data);
            double standardDeviation = calc.sellStandardDeviation();
            double mean = calc.buyAverageRate();

            if(standardDeviation == -1 || mean == -1){
                resultArea.setText(RESULT_ERROR);
            } else {
                String result = new StringBuilder("Odchylenie standardowe: ")
                        .append(doubleFormatter.format(standardDeviation))
                        .append("\nSrednia: ").append(doubleFormatter.format(mean)).toString();
                resultArea.setText(result);
            }
        } else{
            resultArea.setText(VALIDATION_ERROR);
        }
    }

    private void prepareCurrencyTables(){
        currencyTables = new ArrayList<>(
                Arrays.asList(new CurrencyTable('A'),
                        new CurrencyTable('B'),
                        new CurrencyTable('C')));
        currencyTables.forEach(table -> table.fillTable());
    }
}
