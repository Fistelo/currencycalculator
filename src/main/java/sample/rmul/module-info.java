module sample.rmul {
    exports sample.rmul;
    opens sample.rmul;

    requires java.logging;
    requires jdk.incubator.httpclient;
    requires javafx.controls;
    requires javafx.fxml;
    requires json;
    requires commons.math3;
}