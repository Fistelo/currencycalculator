package sample.rmul.data;

import jdk.incubator.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import sample.rmul.Consts;
import sample.rmul.utilities.HttpRequestUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CurrencyTable {

    private static Logger logger = Logger.getLogger(CurrencyTable.class.getName());

    private final char tableName;
    private List<String> table;

    public CurrencyTable(char tableName){
        table = new ArrayList<>();
        this.tableName = tableName;
    }

    public void fillTable(){
        String getTablesRequest = new StringBuffer(Consts.API_LOCATION)
                .append("exchangerates/tables/").append(tableName).toString();
        HttpResponse<String> response = HttpRequestUtils.getRequest(getTablesRequest);
        if(response.statusCode() == Consts.HTTP_RESPONSE_OK)
            addTableValuesFromResponse(response.body());
        else
            logger.log(Level.WARNING, "Data was not downloaded from the server.");
    }

    private void addTableValuesFromResponse(String response){
        JSONArray currencyUnits = new JSONArray(response).getJSONObject(0).getJSONArray("rates");
        for(int i =0 ; i<currencyUnits.length() ; i++){
            JSONObject currency = currencyUnits.getJSONObject(i);
            table.add(currency.getString("code"));
        }
    }

    public List<String> getTable() {
        return table;
    }
    public char getTableName() { return tableName; }
}
