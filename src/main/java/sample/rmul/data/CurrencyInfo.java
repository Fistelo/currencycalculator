package sample.rmul.data;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CurrencyInfo {

    private final LocalDate dateFrom;
    private final LocalDate dateTo;
    private final String currencyCode;
    private List<Character> tableNames;

    public CurrencyInfo(LocalDate dateFrom, LocalDate dateTo, String currencyCode, List<CurrencyTable> acceptableCurrencies) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.currencyCode = currencyCode;
        this.tableNames = getCurrencyTableName(acceptableCurrencies);
    }

    public boolean isDataValidated(){
        if(Stream.of(dateFrom, dateTo, currencyCode).anyMatch(Objects::isNull)
                || tableNames.isEmpty() || dateFrom.isAfter(dateTo) )
            return false;
        return true;
    }

    private List<Character> getCurrencyTableName(List<CurrencyTable> currencyTables){
        return currencyTables.stream()
                .filter(curr -> curr.getTable().contains(currencyCode))
                .map(curr->curr.getTableName())
                .collect(Collectors.toList());
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public List<Character> getTableNames() {
        return tableNames;
    }
}
