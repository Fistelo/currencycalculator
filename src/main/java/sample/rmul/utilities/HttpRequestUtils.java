package sample.rmul.utilities;

import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HttpRequestUtils {

    private static final Logger logger = Logger.getLogger(HttpRequestUtils.class.getName());

    public static HttpResponse<String> getRequest(String url)  {
        HttpRequest request = HttpRequest.newBuilder()
        .uri(buildUri(url))
        .GET()
        .build();

        return send(request);
    }

    private static URI buildUri(String url){
        URI uri = null;
        try {
            uri = new URI(url);
        } catch (URISyntaxException e) {
            logger.log(Level.WARNING, e.toString(), e);
        }
        return uri;
    }

    private static HttpResponse<String> send(HttpRequest request) {
        HttpResponse<String> response = null;
        try {
            response = HttpClient.newBuilder()
                    .build()
                    .send(request, HttpResponse.BodyHandler.asString());

        } catch (IOException | InterruptedException e) {
            logger.log(Level.WARNING, e.toString(), e);
        }
        return response;
    }
}
